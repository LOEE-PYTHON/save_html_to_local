# 使用官方 Python 3.9 镜像
FROM python:3.10-slim


# ENV http_proxy=http://10.10.10.63:7890 \
#     https_proxy=http://10.10.10.63:7890 \
#     no_proxy=localhost,127.0.0.1,.docker.internal
# 安装系统依赖
RUN apt-get update && \
    apt-get install -y \
    pkg-config \
    gcc \
    wget \
    python3-dev \
    libmariadb-dev-compat \
    libmariadb-dev && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# 设置工作目录
WORKDIR /app

# 复制 requirements.txt 和 Django 项目到容器
COPY requirements.txt /app/
COPY . /app/

# 安装依赖
RUN pip install --no-cache-dir -r requirements.txt -i https://pypi.mirrors.ustc.edu.cn/simple/

# 运行 Django 迁移
#RUN python manage.py migrate

# 开放端口
EXPOSE 8000

# 启动 Django 开发服务器
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
