"""
URL configuration for shtl project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,re_path
from shtl_index import views
from django.conf import settings
from django.conf.urls.static import static
import os
from pathlib import Path
BASE_DIR = Path(__file__).resolve().parent.parent

urlpatterns = [
    # path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('add', views.add, name='add'),
    path('server_info', views.server_info, name='server_info'),
    path('login', views.login_view, name='login'),
    path('logout', views.user_logout, name='user_logout'),
    path('change_password', views.change_password, name='change_password'),
    path('search', views.search, name='search'),
    path('delete_static_file/<int:id>', views.delete_static_file, name='delete_static_file'),
    path('edit_static_file', views.edit_static_file, name='edit_static_file'),
    
    path('change_person_info', views.change_person_info, name='change_person_info'),
    path('clear_server', views.clear_server, name='clear_server'),
    
    re_path(r'^nginx/(?P<path>.+)/static/(?P<filename>.+)$', views.serve_static_file, name='serve_static_file'),
    re_path(r'^nginx/(?P<path>.+)/$', views.serve_static_page, name='serve_static_page'),

]


if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=os.path.join(BASE_DIR, "static"))
