from django.apps import AppConfig


class ShtlIndexConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'shtl_index'
