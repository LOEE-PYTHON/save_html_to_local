from django.db import models

# Create your models here.
from django.contrib.auth.models import User  # 如果需要使用用户模型

class StaticFile(models.Model):
    id = models.AutoField(primary_key=True)
    path_name = models.CharField(max_length=255)
    file_name = models.CharField(max_length=255)
    source_url = models.TextField(max_length=5000,default='') 
    entry_name = models.CharField(max_length=255,default='')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.file_name  # 返回文件名作为对象的字符串表示

    class Meta:
        db_table = 'static_file'  # 设置数据库表名

class ServerInfo(models.Model):
    id = models.AutoField(primary_key=True)
    select_type = models.CharField(max_length=255,default="http") # 选择类型协议
    server_name = models.CharField(max_length=255)
    ip = models.CharField(max_length=255)
    port = models.CharField(max_length=255,null=True)
    desc = models.CharField(max_length=255,null=True) #描述
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.file_name  # 返回文件名作为对象的字符串表示

    class Meta:
        db_table = 'server_info'  # 设置数据库表名


class KeyWord(models.Model):
    id = models.AutoField(primary_key=True)
    content = models.TextField()  # 存储替换后的内容
    keywords = models.TextField()  # 存储分词后的关键词，用逗号分隔

    static_file_id = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)


    def __str__(self):
        return self.content
    
    class Meta:
        db_table = 'key_word'  # 设置数据库表名
